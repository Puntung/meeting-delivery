import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import ListScreen from '../screens/ListScreen'
import DeliverScreen from '../screens/DeliverScreen'
import DeliverListScreen from '../screens/DeliverListScreen'
import MyOrderScreen from '../screens/MyOrderScreen'
import MyOrderListScreen from '../screens/MyOrderListScreen'
import AckScreen from '../screens/AckScreen'
const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const HomeStack = createStackNavigator(
  {
    
    Home: HomeScreen,
    Links: LinksScreen,
    List:ListScreen,
    Deliver:DeliverScreen,
    DeliverList:DeliverListScreen,
    MyOrder:MyOrderScreen,
    MyOrderList:MyOrderListScreen,
    Ack:AckScreen,
    
    
  },
  config
);

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarVisible:false
};

const tabNavigator = createBottomTabNavigator({
  HomeStack,
});

tabNavigator.path = '';

export default tabNavigator;
