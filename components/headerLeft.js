import React from 'react';
import { ScrollView, StyleSheet, SafeAreaView, View, Text, TouchableOpacity, AsyncStorage } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import { connect } from 'react-redux'
import { logIn } from '../redux/actions/index'
import {FontAwesome} from '@expo/vector-icons'
class HeaderLeft extends React.Component {
    constructor(props) {
        super(props)
    }
    logOutFunc = async () => {
        let code = await AsyncStorage.getItem('code')
        if (code != null) {
            await AsyncStorage.removeItem('code')
            this.props.logIn('empty')
            this.props.pushToHome()
            console.log('withAsync')
        }
        else {
            this.props.logIn('empty')
            this.props.pushToHome()
            console.log('withOutAync')
        }
    }
    render() {
        return (
            <TouchableOpacity style={{ padding: 5,marginRight:10,flexDirection:'row',alignItems:'center',borderRadius:25 }} onPress={() => this.logOutFunc()}>
                <Text style={{fontFamily:'kanit',color:'white',fontSize:16}}>ออกจากระบบ</Text>
                <FontAwesome name="sign-out" size={20} style={{color:'white',marginLeft:5}} />
            </TouchableOpacity>
        );
    }
}
const mapStateToProps = state => {
    return {
        user: state.redux.user
    }
}
const mapDispatchToProps = dispatch => {
    return {
        logIn: (user) => dispatch(logIn(user))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(HeaderLeft)
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
});