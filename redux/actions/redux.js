import {LOGIN,ROOM_DATA_LIST,ORDER_ROOM,UPDATE_COUNT,PUSH_TOKEN,MY_TOKEN, INITIAL_ACCESS} from './actionTypes'
export const logIn = (user)=>{
    return{
        type:LOGIN,
        user:user
    }
}
export const roomDataList = (room) => {
    return{
        type:ROOM_DATA_LIST,
        room
    }
}
export const orderRoom = (order) => {
    return{
        type:ORDER_ROOM,
        order
    }
}
export const updateCount = () => {
    return{
        type:UPDATE_COUNT,
    }
}
export const pushToken = (token) => {
    return{
        type:PUSH_TOKEN,
        token:token
    }
}
export const myToken = (token) => {
    return{
        type:MY_TOKEN,
        token
    }
}
export const initialAccess = (flag) => {
    return{
        type:INITIAL_ACCESS,
        flag
    }
}