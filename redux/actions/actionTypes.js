export const LOGIN = 'LOGIN'
export const ROOM_DATA_LIST = 'ROOM_DATA_LIST'
export const ORDER_ROOM = 'ORDER_ROOM'
export const UPDATE_COUNT = 'UPDATE_COUNT'
export const PUSH_TOKEN = 'PUSH_TOKEN'
export const MY_TOKEN = 'MY_TOKEN'
export const INITIAL_ACCESS = "INITIAL_ACCESS"