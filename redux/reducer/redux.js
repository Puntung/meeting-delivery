import { LOGIN, ROOM_DATA_LIST, ORDER_ROOM, UPDATE_COUNT, PUSH_TOKEN, MY_TOKEN,INITIAL_ACCESS } from '../actions/actionTypes'
const initialState = {
    name: 'GrabWater',
    user: null,
    roomList: null,
    orderList: null,
    update: 0,
    token: [],
    myToken: '',
    accessFlag:true
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN:
            return {
                ...state,
                user: action.user
            }
        case ROOM_DATA_LIST:
            return {
                ...state,
                roomList: action.room
            }
        case ORDER_ROOM:
            return {
                ...state,
                orderList: action.order
            }
        case UPDATE_COUNT:
            return {
                ...state,
                update: state.update + 1
            }
        case PUSH_TOKEN:
            return {
                ...state,
                token: action.token
            }
        case MY_TOKEN:
            return {
                ...state,
                myToken: action.token
            }
        case INITIAL_ACCESS:
            return{
                ...state,
                accessFlag:action.flag
            }
        default:
            return state;
    }
}

export default reducer