import {createStore,combineReducers} from 'redux';
import redux from './reducer/redux'
const rootReducer = combineReducers({
    redux
})

const configureStore = () => {
    return createStore( rootReducer )
}
export default configureStore