import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import React, { useState } from 'react';
import { Platform, StatusBar, StyleSheet, View, AppRegistry } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import * as Font from 'expo-font';
import { Provider } from 'react-redux';
import AppNavigator from './navigation/AppNavigator';
import configureStore from './redux/configureStore'
import * as firebase from "firebase"
import { Root } from 'native-base'
const firebaseConfig = {
  apiKey: "AIzaSyBjkFelTpEq-CuTRA0OkbjnGPMgPq8wcyY",
  authDomain: "grabwater-ccb85.firebaseapp.com",
  databaseURL: "https://grabwater-ccb85.firebaseio.com",
  projectId: "grabwater-ccb85",
  storageBucket: "grabwater-ccb85.appspot.com",
  messagingSenderId: "1085919241373",
  appId: "1:1085919241373:web:73c0e2fa05f473ef5f2074"
};
firebase.initializeApp(firebaseConfig)
const store = configureStore()
export default class App extends React.Component {
  constructor() {
    super();
    console.ignoredYellowBox = [
      'Setting a timer'
    ];
    this.state = {
      fontsLoaded: false
    }
  }
  testFunc = () => {
    return 1+1
  }
  componentDidMount() {
    // this.testFunc()
    Font.loadAsync({
      'kanit': require('./assets/fonts/Kanit-Regular.ttf'),
      'kanitBold':require('./assets/fonts/Kanit-Bold.ttf'),
      'kanitIt':require('./assets/fonts/Kanit-Italic.ttf')
    }).then(()=>this.setState({fontsLoaded:true}));
  }
  render() {
    if(!this.state.fontsLoaded){
      return <AppLoading />
    }
    return (
      <Provider store={store}>
        <Root>
          <View style={styles.container}>
            <AppNavigator />
          </View>
        </Root>
      </Provider>
    );
  }
}
AppRegistry.registerComponent('App', () => App);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
