import React from 'react';
import {
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  StatusBar,
  AsyncStorage,
  ActivityIndicator,
  Dimensions,
  Alert,
  Keyboard,
  Animated
} from 'react-native';
import Wave from 'react-native-waveview'
import firebase from "firebase"
import '@firebase/firestore'
import { connect } from 'react-redux'
import { logIn, roomDataList, orderRoom, pushToken, initialAccess } from '../redux/actions/index'
import { Notifications } from 'expo'
const { width, height } = Dimensions.get('screen')
class HomeScreen extends React.Component {
  constructor() {
    super();
    console.disableYellowBox = true;
  }
  static navigationOptions = {
    header: null,
  };
  _isMount = false;
  state = {
    code: '',
    response: 'response',
    loading: false,
    borderAnimated: new Animated.Value(0),
    borderFade: new Animated.Value(1),
    ballMove: new Animated.Value(0),
    flood: new Animated.Value(0),
  }
  animated = () => {
    Animated.sequence([
      Animated.timing(this.state.flood, {
        toValue: height * 0.75,
        duration: 1500
      }),
      Animated.loop(
        Animated.parallel([
          Animated.timing(this.state.borderAnimated, {
            toValue: 1,
            duration: 1500
          }),
          Animated.timing(this.state.borderFade, {
            toValue: 0,
            duration: 1500
          })
        ])
      )
    ]).start()


  }
  getRoom = async () => {
    var db = firebase.firestore()
    var roomList = []
    var roomOrder = []
    await db.collection('room').get().then(querySnapshot => {
      querySnapshot.docs.forEach(doc => {
        roomList.push({ 'id': doc.id, 'info': doc.data() })
      })
    })

    this.props.orderRoom(roomOrder)
    // Fetch room which have order.
    for (var i = 0; i < roomList.length; i++) {
      await db.collection("room").doc(roomList[i].id).collection('info').get().then(function (querySnapshot) {
        querySnapshot.forEach(async function (doc) {
          // doc.data() is never undefined for query doc snapshots
          roomOrder.push(doc.data())
        });
      })
    }
    this.props.roomDataList(roomList)
  }
  async componentDidMount() {
    this._isMount = true;
    let hop = this
    var db = firebase.firestore()
    let code = await AsyncStorage.getItem('code')
    if (this._isMount) {
      if (this.props.accessFlag == true) {
        await this.getRoom();
        this.props.initialAccess(false)
      }
      if (code != null) {
        hop.animated()
        db.collection(code).doc('userInfo').get().then(function (doc) {
          if (doc.exists) {
            if (doc.data().position == 'order') {
              hop.props.logIn(doc.data())

              hop.props.navigation.replace('Links')
              hop.setState({
                loading: false
              })
            }
            else {
              hop.props.logIn(doc.data())
              hop.props.navigation.replace('Deliver')
              hop.setState({
                loading: false
              })
            }
          }
          else {
            hop.setState({
              loading: false
            })
          }
        }).catch(function (e) {
        });
      }
      else {
        this.setState({
          loading: false
        }, () => { this.animated() })
      }




      db.collection('expoToken').doc('Deliver').get().then(doc => {
        if (doc.exists) {
          let token = doc.data().token
          this.props.pushToken(token)
        }
      })
      this._notiSubscribe = Notifications.addListener((noti) => this.NotificationStore(noti))
    }
  }
  componentWillUnmount() {
    this._isMount = false;
  }
  NotificationStore = async (noti) => {
    let db = firebase.firestore()
    let roomOrder = []
    for (var i = 0; i < this.props.roomList.length; i++) {
      await db.collection("room").doc(this.props.roomList[i].id).collection('info').get().then(function (querySnapshot) {
        querySnapshot.forEach(async function (doc) {
          // doc.data() is never undefined for query doc snapshots
          roomOrder.push(doc.data())
        });
      })
    }
    this.props.orderRoom(roomOrder)
  }
  Login = async () => {
    Keyboard.dismiss()
    this.setState({
      loading: true
    })
    if (this.state.code == "") {
      this.setState({
        loading: false
      })
      return alert("กรุณากรอกหมายเลขเพื่อเข้าใช้งาน")
    }
    let hop = this
    var db = firebase.firestore();
    db.collection(this.state.code).doc('userInfo').get().then(function (doc) {
      if (doc.exists) {
        Alert.alert(
          'LOGIN',
          'คุณต้องการจำหมายเลขนี้หรือไม่',
          [
            {
              text: 'ต้องการ', onPress: () => {
                AsyncStorage.setItem('code', hop.state.code)
                hop.props.logIn(doc.data())
                if (doc.data().position == 'order') {
                  hop.props.logIn(doc.data())
                  hop.props.navigation.replace('Links')
                  hop.setState({
                    loading: false
                  })
                }
                else {
                  hop.props.logIn(doc.data())
                  hop.props.navigation.replace('Deliver')
                  hop.setState({
                    loading: false
                  })
                }
              }
            },
            {
              text: 'ไม่ต้องการ', onPress: () => {
                if (doc.data().position == 'order') {
                  hop.props.logIn(doc.data())
                  hop.props.navigation.replace('Links')
                  hop.setState({
                    loading: false
                  })
                }
                else {
                  hop.props.logIn(doc.data())
                  hop.props.navigation.replace('Deliver')
                  hop.setState({
                    loading: false
                  })
                }
              }
            }
          ]
        )
        // console.log("Data =>", doc.data())

      }
      else {
        alert("หมายเลขไม่ถูกต้อง")
        hop.setState({
          loading: false
        })
      }
    }).catch(function (e) {
      hop.setState({
        loading: false
      })
    });
  }
  InputCode = (text) => {
    this.setState({
      code: text
    })
  }
  render() {
    return (
      <View style={styles.container}>
        {
          this.state.loading &&
          <View style={{ position: 'absolute', left: 0, top: 0, zIndex: 100, width, height, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(255, 255, 255, 0.8)' }}>
            <ActivityIndicator color="black" size="large" />
          </View>
        }
        <Animated.View style={{ position: 'absolute', bottom: 0, zIndex: 80, transform: [{ translateY: this.state.flood }] }}>
          <Wave
            style={styles.waveBall}
            H={height * 0.6}
            waveParams={[
              { A: 40, T: width, fill: '#62c2ff' },
              { A: 35, T: width, fill: '#0087dc' },
              { A: 45, T: width, fill: '#1aa7ff' },
            ]}
            animated={true}
          />
        </Animated.View>

        <View style={{ width: width, alignItems: 'center', height: 200, paddingTop: 20, backgroundColor: '#F6511D' }}>
          <Image source={require('../assets/images/wtLogo.png')} style={{ width: width * 0.8, height: width * 0.36 }} />
        </View>
        <View style={{ justifyContent: 'space-around', width: width, alignItems: 'center', height: 300, marginTop: 50, backgroundColor: 'white', }}>
          <View style={{ justifyContent: 'center', alignItems: 'center', position: 'absolute', borderWidth: 1, borderRadius: 30, padding: 15, top: -90, backgroundColor: 'white', borderColor: 'white' }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: 200, position: 'absolute', top: 10, zIndex: 10, overflow: 'hidden' }}>
            </View>
            <Text style={{ fontSize: 24, fontFamily: 'kanit', color: '#006494' }}>WUNDERDRINK</Text>
          </View>
          <TextInput autoCapitalize="none" placeholder="หมายเลขของคุณ" value={this.state.code} onChangeText={(text) => this.InputCode(text)} style={{ borderBottomWidth: 0.5, width: '70%', fontFamily: 'kanit' }} />
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Animated.View style={{ width: 200, height: 200, borderRadius: 200 / 2, borderWidth: 1, borderColor: '#119DA4', position: 'absolute', transform: [{ scale: this.state.borderAnimated }], opacity: this.state.borderFade }}>

            </Animated.View >
            <TouchableOpacity onPress={() => this.Login()} style={{ width: 100, backgroundColor: '#119DA4', height: 100, alignItems: 'center', justifyContent: 'center', borderRadius: 100 / 2 }}>
              <Text style={{ color: 'white', fontFamily: 'kanit' }}>
                เข้าสู่ระบบ
            </Text>
            </TouchableOpacity>
          </View>
        </View>
        <Text style={{ position: 'absolute', bottom: 35, fontFamily: 'kanit', fontSize: 12, color: '#787878' }}>Develop by Pond Nakarin</Text>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    name: state.redux.name,
    user: state.redux.user,
    roomList: state.redux.roomList,
    orderList: state.redux.orderList,
    accessFlag: state.redux.accessFlag
  }
}
const mapDispatchToProps = dispatch => {
  return {
    logIn: (user) => dispatch(logIn(user)),
    roomDataList: (room) => dispatch(roomDataList(room)),
    orderRoom: (order) => dispatch(orderRoom(order)),
    pushToken: (token) => dispatch(pushToken(token)),
    initialAccess: (flag) => dispatch(initialAccess(flag))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: StatusBar.currentHeight
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  waveBall: {
    width: width,
    aspectRatio: 1,
    alignSelf: 'center',
    backgroundColor: 'rgba(255,255,0,0)',
  }
});
