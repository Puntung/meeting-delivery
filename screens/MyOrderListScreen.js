import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    StatusBar,
    AsyncStorage,
    ActivityIndicator,
    Dimensions,
    Alert,
    Keyboard,
    SafeAreaView
} from 'react-native';
import { Input, Form, Item, Label, Card, CardItem, Body, Icon, Toast } from 'native-base'
import firebase from "firebase"
import '@firebase/firestore'
import { connect } from 'react-redux'
import { FontAwesome } from '@expo/vector-icons'
import HeaderLeft from '../components/headerLeft'
import { logIn, orderRoom, updateCount } from '../redux/actions/index'
import { FlatList } from 'react-native-gesture-handler';
const { width, height } = Dimensions.get('screen')
class MyOrderListScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerStyle: {
                backgroundColor: "#610085"
            },
            headerLeft: <TouchableOpacity style={{ padding: 8, marginLeft: 5, flexDirection: 'row', alignItems: 'center',  borderRadius: 25 }} onPress={() => navigation.pop()}>
                <FontAwesome name="angle-left" size={20} style={{color:'white'}}/>
                <Text style={{ fontSize: 15, marginLeft: 5, fontFamily: 'kanit' ,color:'white'}}>กลับ</Text>
            </TouchableOpacity>,
            headerTitle: <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}><Text style={{ fontSize: 20, fontFamily: 'kanit', color: 'white' }}>รายการสั่งของฉัน</Text></View>,
            headerRight: <View></View>
        };
    }
    state = {
        orderList: this.props.navigation.state.params.orderList,
        loading: false
    }
    deleteOrder = () => {
        this.setState({
            loading: true
        })
        let db = firebase.firestore()
        let hop = this
        db.collection("room").doc(hop.state.orderList.OrderRoom).collection('info').doc(hop.state.orderList.OrderDate + "-" + hop.state.orderList.OrderTime).delete().then(async function () {
            hop.setState({
                loading: false
            })
            alert('ลบคำสั่งเรียบร้อย')
            let FtAtDl = hop.props.orderList.filter(item => item.OrderDate + "-" + item.OrderTime != hop.state.orderList.OrderDate + "-" + hop.state.orderList.OrderTime)
            console.log(FtAtDl)
            hop.props.orderRoom(FtAtDl)
            hop.props.navigation.pop()
        }).catch(function (error) {
            hop.setState({
                loading: false
            })
            alert("เกิดข้อผิดพลาด กรุณาทำใหม่อีกครั้ง")
            hop.props.navigation.pop()
        });


    }
    headerComp = () => {
        return (
            <View style={{ borderTopWidth: 55, borderColor: '#db63aa' }}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={{ fontSize: 60, fontFamily: 'kanit', color: '#0A2463' }}>{this.state.orderList.OrderRoom}</Text>
                </View>
            </View>
        )
    }
    footerComp = () => {
        return (
            <View style={{ marginHorizontal: 10, marginTop: 40 }}>
                <View style={{ width: width * 0.9, borderBottomWidth: 0.5, alignSelf: 'center', marginBottom: 20 }}>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', margin: 5 }}>
                    <Text style={{ fontSize: 15, fontFamily: 'kanit',color:'#0A2463' }}>วันที่จัดส่ง:</Text>
                    <View style={{ marginLeft: 10, borderBottomWidth: 0.5 }}>
                        <Text style={{ fontSize: 15, fontFamily: 'kanit',color:'grey' }}>{this.state.orderList.OrderDate}</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', margin: 5 }}>
                    <Text style={{ fontSize: 15, fontFamily: 'kanit' ,color:'#0A2463'}}>เวลาที่จัดส่ง:</Text>
                    <View style={{ marginLeft: 10 }}>
                        <Text style={{ fontSize: 15, color: 'grey', fontFamily: 'kanit' }}>{this.state.orderList.OrderTime}</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', margin: 5 }}>
                    <Text style={{ fontSize: 15, fontFamily: 'kanit' ,color: '#0A2463'}}>ชื่อผู้สั่ง:</Text>
                    <View style={{ marginLeft: 10 }}>
                        <Text style={{ fontSize: 15, color: 'grey', fontFamily: 'kanit' }}>{this.state.orderList.OrderUser}</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', margin: 5 }}>
                    <Text style={{ fontSize: 15, fontFamily: 'kanit',color: '#0A2463' }}>ชื่อผู้รับผิดชอบ:</Text>
                    <View style={{ marginLeft: 10 }}>
                        <Text style={{ fontSize: 15, color: 'grey', fontFamily: 'kanit' }}>{this.state.orderList.DeliverUser == 'empty' ? 'ยังไม่มีผู้รับผิดชอบ' : this.state.orderList.DeliverUser}</Text>
                    </View>
                </View>
                <Text style={{ fontSize: 15, margin:5, fontFamily: 'kanit',color:'#0A2463' }}>รายการเพิ่มเติม</Text>
                <View style={styles.textAreaContainer}>
                    <TextInput
                        style={styles.textArea}
                        multiline={true}
                        numberOfLines={4}
                        placeholder={'ไม่ได้ระบุ'}
                        placeholderTextColor="grey"
                        editable={false}
                        value={this.state.orderList.OtherOrder} />
                </View>
            </View>
        )
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                {
                    this.state.loading &&
                    <View style={{ position: 'absolute', left: 0, top: 0, zIndex: 8, width, height, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(255, 255, 255, 0.8)' }}>
                        <ActivityIndicator color="black" size="large" />
                    </View>
                }
                <FlatList
                    data={this.state.orderList.OrderList}
                    extraData={this.state}
                    ListFooterComponent={this.footerComp}
                    keyExtractor={(item, index) => index.toString()}
                    ListHeaderComponent={this.headerComp}
                    renderItem={({ item, index }) =>
                        <View key={index}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', margin: 10 }}>
                                <Text style={{ fontSize: 15, color: '#0A2463', fontFamily: 'kanit' }}>{item.OrderName}</Text>
                                <Text style={{ fontSize: 15, fontFamily: 'kanit', color: '#0A2463' }}>{item.Amount} ชิ้น</Text>
                            </View>
                        </View>
                    }
                />
                <View style={{ justifyContent: 'center', alignitems: 'center', height: 55 ,backgroundColor:'#db63aa'}}>
                <TouchableOpacity onPress={() => this.deleteOrder()} style={{ backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', padding: 5, width: width * 0.5, alignSelf: 'center', borderRadius: 30,  flexDirection: 'row' }}>
                        <Text style={{ fontSize: 16, color: '#0A2463', fontFamily: 'kanit' }}>ยกเลิกรายการสั่ง</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    textAreaContainer: {
        borderColor: '#666',
        borderWidth: 1,
        padding: 5,
        marginTop: 10
    },
    textArea: {
        height: 150,
        textAlignVertical: 'top', fontFamily: 'kanit'
    }
})
const mapStateToProps = state => {
    return {
        user: state.redux.user,
        roomList: state.redux.roomList,
        orderList: state.redux.orderList
    }
}
const mapDispatchToProps = dispatch => {
    return {
        logIn: (user) => dispatch(logIn(user)),
        orderRoom: (order) => dispatch(orderRoom(order)),
        updateCount: () => dispatch(updateCount())
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(MyOrderListScreen)