import React from 'react';
import { StyleSheet, SafeAreaView, View, Text, TouchableOpacity, AsyncStorage, FlatList, ActivityIndicator, Dimensions, TextInput } from 'react-native';
import { connect } from 'react-redux'
import { logIn } from '../redux/actions/index'
import firebase, { firestore } from "firebase"
import '@firebase/firestore'
import { Picker } from 'native-base'
import DatePicker from 'react-native-datepicker'
import { FontAwesome } from '@expo/vector-icons'
import Icon from '@expo/vector-icons/Ionicons';
import TimePicker from 'react-native-24h-timepicker'
const { width, height } = Dimensions.get('screen')
class ListScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerStyle: {
                backgroundColor: "#610085"
            },
            headerLeft: <TouchableOpacity style={{ padding: 8, marginLeft: 5, flexDirection: 'row', alignItems: 'center', borderRadius: 25 }} onPress={() => navigation.pop()}>
                <FontAwesome name="angle-left" size={20} style={{ color: 'white' }} />
                <Text style={{ fontSize: 15, marginLeft: 5, fontFamily: 'kanit', color: 'white' }}>กลับ</Text>
            </TouchableOpacity>,
            headerTitle: <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}><Text style={{ fontSize: 20, fontFamily: 'kanit', color: 'white' }}>เลือกรายการ</Text></View>,
            headerRight: <View></View>
        };
    }
    state = {
        roomSelect: this.props.navigation.state.params.roomSelect,
        OrderAmount: 1,
        OrderList: [
            {
                OrderName: 'เลือกรายการ',
                Amount: 1
            }
        ],
        OtherOrder: "",
        date: '',
        time: 'กรุณาเลือกเวลาจัดส่ง',
        loading: false,
        maxDate:''
    }
    changeValue = (item, index) => {
        let temp = this.state.OrderList
        temp[index].OrderName = item
        this.setState({
            OrderList: temp
        }, () => this.addOrderAmount())
    }
    orderSave = async () => {
        this.setState({
            loading: true
        })
        let db = firebase.firestore()
        let hop = this
        if (this.state.OrderList[0].OrderName == 'เลือกรายการ') {
            this.setState({
                loading: false
            })
            return alert('กรุณาเลือกรายการที่ต้องการสั่ง')
        }
        else if (this.state.date == 'กรุณาเลือกวันที่จัดส่ง' || this.state.time == 'กรุณาเลือกเวลาจัดส่ง') {
            this.setState({
                loading: false
            })
            return alert('กรุณาเลือกวันที่และเวลา')
        }
        var orderWithoutTail = this.state.OrderList
        await db.collection("room").doc(hop.state.roomSelect.id).collection('info').doc(hop.state.date + "-" + hop.state.time).get().then(function (doc) {
            if (doc.exists) {
                alert('มีการใช้งานห้องประชุมในช่วงเวลานี้แล้ว')
                hop.setState({
                    loading: false
                })
            } else {
                orderWithoutTail.pop()
                console.log('orderWithoutTail =>', orderWithoutTail)
                db.collection('room').doc(hop.state.roomSelect.id).collection('info').doc(hop.state.date + "-" + hop.state.time).set({
                    'DeliverUser': 'empty',
                    'OrderList': orderWithoutTail,
                    'orderStatus': true,
                    'OrderUser': hop.props.user.name,
                    'OtherOrder': hop.state.OtherOrder,
                    'OrderDate': hop.state.date,
                    'OrderTime': hop.state.time,
                    'OrderRoom': hop.state.roomSelect.id,
                    'PushToken': hop.props.myToken,
                    'DeliverStatus': false
                })
                    .then(function () {
                        hop.setState({
                            loading: false
                        })
                        console.log('this token =>', hop.props.token)
                        fetch('https://exp.host/--/api/v2/push/send', {
                            method: 'POST',
                            headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                to: hop.props.token,
                                title: 'New Order',
                                body: `มีคำสั่งใหม่จากห้อง ${hop.state.roomSelect.id}`,
                                sound: 'default'
                            }),
                        });
                        alert('คุณได้สั่งรายการเรียบร้อยแล้ว')
                        //hop.props.navigation.pop()
                    })
            }
        }).catch(function (error) {
            console.log("Error getting document:", error);
        });

    }
    addOrderAmount = () => {
        let temp = this.state.OrderList
        if (this.state.OrderList[this.state.OrderList.length - 1].OrderName == 'เลือกรายการ') {
            alert('กรุณาเลือกรายการก่อนเพิ่มรายการ')
        }
        else {
            let body = {
                OrderName: 'เลือกรายการ',
                Amount: 1
            }
            temp.push(body)
            this.setState({
                OrderList: temp
            }, () => console.log(this.state.OrderList))
        }
    }
    SelectTime = async (hour, minute) => {
        if (hour < 7) {
            return alert('ไม่สามารถเลือกช่วงเวลานั้นได้')
        }
        this.setState({
            time: hour + ':' + minute
        })
        this.TimePicker.close()
    }
    SelectDate = async (date) => {
        // console.log('Date =>', date.toString())
        // let pureDate = date.toString().split(' ')
        // switch (pureDate[1]) {
        //     case 'Jan':
        //         pureDate[1] = '01'
        //         break;
        //     case 'Feb':
        //         pureDate[1] = '02'
        //         break;
        //     case 'Mar':
        //         pureDate[1] = '03'
        //         break;
        //     case 'Apr':
        //         pureDate[1] = '04'
        //         break;
        //     case 'May':
        //         pureDate[1] = '05'
        //         break;
        //     case 'Jun':
        //         pureDate[1] = '06'
        //         break;
        //     case 'Jul':
        //         pureDate[1] = '07'
        //         break;
        //     case 'Aug':
        //         pureDate[1] = '08'
        //         break;
        //     case 'Sep':
        //         pureDate[1] = '09'
        //         break;
        //     case 'Oct':
        //         pureDate[1] = '10'
        //         break;
        //     case 'Nov':
        //         pureDate[1] = '11'
        //         break;
        //     case 'Dec':
        //         pureDate[1] = '12'
        //         break;
        // }
        this.setState({
            date: date
        })

    }
    changeAmount = (status, index) => {
        if (status == 'add') {
            let temp = this.state.OrderList
            temp[index].Amount = temp[index].Amount + 1
            this.setState({
                OrderList: temp
            })
        }
        else if (status == 'del' && this.state.OrderList[index].Amount != 1) {
            let temp = this.state.OrderList
            temp[index].Amount = temp[index].Amount - 1
            this.setState({
                OrderList: temp
            })
        }
    }
    headerComp = () => {
        return (
            <View style={{ borderTopWidth: 40, borderColor: '#db63aa' }}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={{ fontSize: 60, fontFamily: 'kanit', color: '#0A2463' }}>{this.state.roomSelect.id}</Text>
                </View>
            </View>
        )
    }
    footerComp = () => {
        return (
            <View style={{ marginHorizontal: 10, marginTop: 40 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ fontSize: 15, fontFamily: 'kanit', color: '#0A2463' }}>วันที่จัดส่ง:</Text>
                    {/* <DatePicker
                        defaultDate={new Date()}
                        minimumDate={new Date()}
                        locale={"en"}
                        timeZoneOffsetInMinutes={undefined}
                        modalTransparent={false}
                        animationType={"fade"}
                        androidMode={"default"}
                        placeHolderText="กรุณาเลือกวันที่จัดส่ง"
                        textStyle={{ color: "grey", fontSize: 15,fontFamily:'kanit' }}
                        placeHolderTextStyle={{ color: "grey", fontSize: 15 ,fontFamily:'kanit'}}
                        onDateChange={this.SelectDate}
                        disabled={false}
                    /> */}
                    <DatePicker
                        placeholder="กรุณาเลือกวันที่จัดส่ง"
                        date={this.state.date}
                        mode="date"
                        style={{width:200}}
                        minDate={this.state.maxDate}
                        format="DD-MM-YYYY"
                        confirmBtnText="ตกลง"
                        cancelBtnText="ยกเลิก"
                        onDateChange={(date) => this.SelectDate(date)}
                        customStyles={{
                            dateIcon:{
                                display:'none'
                            },
                            dateInput:{
                                color:'grey',
                                fontSize:15,
                                borderWidth:0
                            },
                            placeholderText:{
                                color:'grey',
                                fontFamily:'kanit',
                                fontSize:15
                            }
                        }}
                    />
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontSize: 15, fontFamily: 'kanit', color: '#0A2463' }}>เวลาที่จัดส่ง:</Text>
                    <TouchableOpacity onPress={() => this.TimePicker.open()} style={{ marginLeft: 10 }}>
                        <Text style={{ fontSize: 15, color: 'grey', fontFamily: 'kanit' }}>{this.state.time}</Text>
                    </TouchableOpacity>
                </View>

                <Text style={{ fontSize: 15, marginTop: 10, fontFamily: 'kanit', color: '#0A2463' }}>รายการเพิ่มเติม</Text>
                <View style={styles.textAreaContainer}>
                    <TextInput
                        style={styles.textArea}
                        multiline={true}
                        numberOfLines={4}
                        placeholder={'ไม่จำเป็นต้องกรอก'}
                        placeholderTextColor="grey"
                        onChangeText={(text) => this.setState({ OtherOrder: text })}
                        value={this.state.OtherOrder} />
                </View>
                <TimePicker
                    ref={ref => {
                        this.TimePicker = ref;
                    }}
                    maxHour={20}
                    onCancel={() => this.TimePicker.close()}
                    onConfirm={(hour, minute) => this.SelectTime(hour, minute)}
                />
            </View>
        )
    }
    async componentDidMount(){
        var date = new Date()
        var month = ("0"+(date.getMonth()+1)).slice(-2)
        var day = ("0"+date.getDate()).slice(-2)
        var fullDate = day+"-"+month+"-"+date.getFullYear()
        console.log("date is",fullDate)
        await this.setState({
            maxDate:fullDate
        })
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                {
                    this.state.loading &&
                    <View style={{ position: 'absolute', left: 0, top: 0, zIndex: 8, width, height, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(255, 255, 255, 0.8)' }}>
                        <ActivityIndicator color="black" size="large" />
                    </View>

                }
                <FlatList
                    data={this.state.OrderList}
                    extraData={this.state}
                    ListFooterComponent={this.footerComp}
                    keyExtractor={(item, index) => index.toString()}
                    ListHeaderComponent={this.headerComp}
                    renderItem={({ item, index }) =>
                        <View key={index}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                                <Picker
                                    selectedValue={item.OrderName}
                                    style={{ height: 50, width: width * 0.5 }}
                                    itemTextStyle={{ fontFamily: 'kanit' }}
                                    textStyle={{ fontFamily: 'kanit' }}
                                    onValueChange={(itemValue, itemIndex) => this.changeValue(itemValue, index)}>
                                    <Picker.Item label="เลือกรายการ" value="เลือกรายการ" />
                                    <Picker.Item label="น้ำเปล่า" value="น้ำเปล่า" />
                                    <Picker.Item label="กาแฟ" value="กาแฟ" />
                                    <Picker.Item label="ชา" value="ชา" />
                                </Picker>
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginRight: 20 }}>
                                    <TouchableOpacity onPress={() => this.changeAmount('del', index)}><Icon name="ios-remove-circle-outline" size={30} /></TouchableOpacity>
                                    <View style={{ width: 40, alignItems: 'center' }}>
                                        <Text style={{ fontSize: 15, fontFamily: 'kanit', color: '#0A2463' }}>{item.Amount}</Text>
                                    </View>
                                    <TouchableOpacity onPress={() => this.changeAmount('add', index)}><Icon name="ios-add-circle-outline" size={30} /></TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    }
                />
                <View style={{ justifyContent: 'center', alignitems: 'center', height: 80, backgroundColor: '#db63aa' }}>
                    <TouchableOpacity onPress={() => this.orderSave()} style={{ backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', padding: 5, width: width * 0.5, alignSelf: 'center', borderRadius: 35, }}>
                        <Text style={{ fontSize: 16, color: '#0A2463', fontFamily: 'kanit' }}>สั่งรายการ</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    textAreaContainer: {
        borderColor: '#666',
        borderWidth: 1,
        padding: 5,
        marginTop: 10
    },
    textArea: {
        height: 150,
        textAlignVertical: 'top'
        , fontFamily: 'kanit'
    }
});
const mapStateToProps = state => {
    return {
        user: state.redux.user,
        roomList: state.redux.roomList,
        token: state.redux.token,
        myToken: state.redux.myToken
    }
}
const mapDispatchToProps = dispatch => {
    return {
        logIn: (user) => dispatch(logIn(user))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ListScreen)