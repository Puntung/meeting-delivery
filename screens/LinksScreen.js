import React from 'react';
import { ScrollView, StyleSheet, SafeAreaView, View, Text, TouchableOpacity, AsyncStorage, FlatList, ActivityIndicator, Dimensions, Image } from 'react-native';
import { ExpoLinksView } from '@expo/samples';
import { connect } from 'react-redux'
import HeaderLeft from '../components/headerLeft'
import { logIn, orderRoom, myToken } from '../redux/actions/index'
import firebase, { storage } from "firebase"
import '@firebase/firestore'
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import { FontAwesome } from '@expo/vector-icons'
import * as ImagePicker from 'expo-image-picker'
var faker = require('faker')
const { width, height } = Dimensions.get('screen')
class LinksScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerStyle: {
        backgroundColor: "#610085"
      },
      headerLeft: <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, marginLeft: 10 }}><Text style={{ fontSize: 20, fontFamily: 'kanit', color: 'white' }}>ห้องประชุม</Text></View>,
      headerTitle: <View></View>,
      headerRight: <HeaderLeft pushToHome={() => navigation.replace('Home',{page:'logout'})} logOut={() => this.logOutFunc()} />
    };
  }
  state = {
    roomList: this.props.roomList,
    loading: false,
    avatar: this.props.user.avatar,
    check: true,
  }
  async componentDidMount() {
    const { status: existingStatus } = await Permissions.getAsync(
      Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      // Android remote notification permissions are granted during the app
      // install, so this will only ask on iOS
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
      
    }
    if (finalStatus !== 'granted') {
      return ;
    }
    else if(finalStatus == 'granted') {
      try{
      let token = await Notifications.getExpoPushTokenAsync();
      this.props.myToken(token)
      console.log('token is => ',token)
      }
      catch(e){
        alert(e)
      }
    }
  }
  logOutFunc = async () => {
    let code = await AsyncStorage.getItem('code')
    if (code != null) {
      await AsyncStorage.removeItem('code')
      this.props.logIn(null)
      this.props.navigation.replace('Home')
    }
    else {
      this.props.logIn(null)
      this.props.navigation.replace('Home')
    }
  }
  SelectRoom = (item) => {
    if (item.info.OrderStatus) {
      alert('ห้องนี้กำลังถูกใช้งาน')
    }
    else {
      this.props.navigation.push('List', { roomSelect: item })
    }
  }
  myOrder = async () => {

    this.props.navigation.push('MyOrder')
  }
  uriToBlob = (uri) => {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.onload = function () {
        // return the blob
        resolve(xhr.response);
      };

      xhr.onerror = function () {
        // something went wrong
        reject(new Error('uriToBlob failed'));
      };
      // this helps us get a blob
      xhr.responseType = 'blob';
      xhr.open('GET', uri, true);

      xhr.send(null);
    });
  }
  uploadToFirebase = (blob) => {

    return new Promise((resolve, reject) => {

      var storageRef = firebase.storage().ref('images');

      storageRef.child('uploads/photo.jpg').put(blob, {
        contentType: 'image/jpeg'
      }).then((snapshot) => {

        blob.close();
        alert(success)
        resolve(snapshot);

      }).catch((error) => {
        alert(error)
        reject(error);

      });

    });


  }
  changeAvatar = async () => {
    this.setState({
      loading: false
    })
    var storageRef = firebase.storage().ref()
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status !== 'granted') {
      alert('กรุณาอนุญาตก่อนเข้าใช้งาน');
    }
    else {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1
      });

      console.log(result);

      if (!result.cancelled) {
        var blobImg = await this.uriToBlob(result.uri)
        // alert(JSON.stringify(blobImg))
        try {
          var uploadTask = storageRef.child('image'+this.props.user.id).put(blobImg, {
            contentType: 'image/jpeg'
          }).then((snapshot) => {
            
          }).catch((e)=>{
            alert(JSON.stringify(e))
          })

        }
        catch (e) {
          alert(JSON.stringify(e))
          
        }

      }
    }
  }
  headerComp = () => {
    return (
      <View style={{ backgroundColor: '#db63aa' }}>
        <Image source={require('../assets/images/bgOrder.jpg')} style={{ width: width, height: 200,zIndex:-2,position:'absolute',opacity:0.1}} />
        <View style={{ justifyContent: 'flex-start', flexDirection: 'row', marginTop: 20, height: 200, marginLeft: 25 }}>
          
          {
            this.state.avatar != "" ?
              <Image source={{ uri: this.state.avatar }} style={{ width: 100, height: 100, borderRadius: 100 / 2 }} />
              :
              <FontAwesome name="user-circle" size={100} style={{ color: 'white' ,textShadowOffset:{width: 1, height: 1},textShadowRadius:1}} />
          }
          <View style={{ marginLeft: 25 }}>
            <Text style={{ fontFamily: 'kanit', fontSize: 20, color: 'white' ,textShadowOffset:{width: 5, height: 5},textShadowRadius:10}}>{this.props.user.name}</Text>
            <Text style={{ fontFamily: 'kanit', fontSize: 15, color: 'white' ,textShadowOffset:{width: 5, height: 5},textShadowRadius:10}}>ตำแหน่ง : {this.props.user.position}</Text>
            {/* <TouchableOpacity onPress={() => this.changeAvatar()}>
              <Text style={{ fontFamily: 'kanit', fontSize: 15, color: 'white', textDecorationLine: 'underline' ,textShadowOffset:{width: 5, height: 5},textShadowRadius:10}}>เปลี่ยนรูประจำตัว</Text>
            </TouchableOpacity> */}
          </View>
        </View>
        <View style={{ bottom: -15, position: 'absolute', flexDirection: 'row', justifyContent: 'center', borderRadius: 20, backgroundColor: 'white', width: width, paddingBottom: 50, paddingTop: 20 }}>
          <Text style={{ fontSize: 15, fontFamily: 'kanit', color: '#686868' }}>กรุณาเลือกห้องประชุม</Text>
        </View>
      </View>
    )
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        {
          this.state.loading &&
          <View style={{ position: 'absolute', left: 0, top: 0, zIndex: 8, width, height, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(255, 255, 255, 0.8)' }}>
            <ActivityIndicator color="black" size="large" />
          </View>

        }


        <FlatList
          numColumns={3}
          ListHeaderComponent={this.headerComp}
          data={this.state.roomList}
          keyExtractor={(item, index) => item.id}
          renderItem={({ item, index }) =>
            <TouchableOpacity onPress={() => this.SelectRoom(item)} style={{ justifyContent: 'center', alignItems: 'center', width: width * 0.25, height: width * 0.25, borderRadius: 10, backgroundColor: '#db63aa', flex: 1, margin: 5,borderWidth:3,borderColor:'#610085' }} key={index}>
              <Text style={{ fontSize: 60, color: 'white', fontFamily: 'kanit' }}>{item.id}</Text>
            </TouchableOpacity>
          }
        />
        <View style={{ justifyContent: 'center', alignitems: 'center', height: 80, borderTopWidth: 0.5, width: width * 0.8, alignSelf: 'center' }}>
          <TouchableOpacity onPress={() => this.myOrder()} style={{ backgroundColor: '#db63aa', justifyContent: 'center', alignItems: 'center', padding: 10, width: width * 0.5, alignSelf: 'center', borderRadius: 20 }}>
            <Text style={{ fontSize: 16, color: 'white', fontFamily: 'kanit' }}>รายการสั่งของฉัน</Text>
          </TouchableOpacity>
        </View>

      </SafeAreaView>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.redux.user,
    roomList: state.redux.roomList
  }
}
const mapDispatchToProps = dispatch => {
  return {
    logIn: (user) => dispatch(logIn(user)),
    orderRoom: (order) => dispatch(orderRoom(order)),
    myToken: (token) => dispatch(myToken(token))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(LinksScreen)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
