import React from 'react';
import {
    Image,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    AsyncStorage,
    ActivityIndicator,
    Dimensions,
    SafeAreaView,
    RefreshControl
} from 'react-native';
import { Input, Form, Item, Label, Card, CardItem, Body, Icon, Toast } from 'native-base'
import firebase from "firebase"
import '@firebase/firestore'
import { connect } from 'react-redux'
import HeaderLeft from '../components/headerLeft'
import { logIn, orderRoom } from '../redux/actions/index'
import { FlatList } from 'react-native-gesture-handler';
import { FontAwesome } from '@expo/vector-icons';
const { width, height } = Dimensions.get('screen')
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
var faker = require('faker')
class DeliverScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerStyle: {
                backgroundColor: "#0A2463"
            },
            headerLeft: <View style={{ marginLeft: 10, justifyContent: 'center', alignItems: 'center', flex: 1 }}><Text style={{ color: 'white', fontSize: 20, fontFamily: 'kanit' }}>รายการที่ถูกสั่ง</Text></View>,
            headerTitle: <View></View>,
            headerRight: <HeaderLeft pushToHome={() => navigation.replace('Home',{page:'logout'})} logOut={() => this.logOutFunc()} />
        };
    }
    state = {
        roomList: this.props.roomList,
        loading: false,
        refreshing: false,
        filterMyOrder: false,
        avatar: faker.image.avatar()
    }
    logOutFunc = async () => {
        let code = await AsyncStorage.getItem('code')
        if (code != null) {
            await AsyncStorage.removeItem('code')
            this.props.logIn(null)
            this.props.navigation.replace('Home')
        }
        else {
            this.props.logIn(null)
            this.props.navigation.replace('Home')
        }
    }
    myOrder = () => {
        if (this.state.filterMyOrder == false) {
            this.setState({
                orderList: this.props.orderList
            })
            let filter = this.props.orderList.filter(item => (item.DeliverUser == this.props.user.name) && (item.DeliverStatus == false))
            this.props.orderRoom(filter)
            this.setState({
                filterMyOrder: true
            })
        }
        else {
            this.props.orderRoom(this.state.orderList)
            this.setState({
                filterMyOrder: false
            })
        }
    }
    headerComp = () => {
        return (
            <View style={{ backgroundColor: '#9de2fc' }}>
                <Image source={require('../assets/images/bgDel.jpg')} style={{width,height:200,position:'absolute',zIndex:-1,opacity:0.2}} />
                <View style={{ justifyContent: 'flex-start', flexDirection: 'row', marginTop: 20, height: 200, marginLeft: 25 }}>
                    <Image source={{ uri: this.state.avatar }} style={{ width: 100, height: 100, borderRadius: 100 / 2 }} />
                    <View style={{ marginLeft: 25 }}>
                        <Text style={{ fontFamily: 'kanit', fontSize: 20, color: 'white' }}>{this.props.user.name}</Text>
                        <Text style={{ fontFamily: 'kanit', fontSize: 15, color: 'white' }}>ตำแหน่ง : {this.props.user.position}</Text>
                    </View>
                </View>
                <View style={{ bottom: -15, position: 'absolute', flexDirection: 'row', justifyContent: 'space-between', borderRadius: 20, backgroundColor: 'white', width: width, paddingBottom: 50, paddingTop: 20 }}>
                    <Text style={{ fontSize: 14, fontFamily: 'kanit', marginLeft: 10 }}>จำนวนงาน : {this.props.orderList.length}</Text>
                    <TouchableOpacity onPress={() => this.myOrder()} style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <FontAwesome name={this.state.filterMyOrder ? 'check-square-o' : 'square-o'} size={20} color={this.state.filterMyOrder ? '#FC5130' : 'black'} />
                        <Text style={{ fontSize: 14, marginRight: 10, fontFamily: 'kanit', color: this.state.filterMyOrder ? '#FC5130' : 'black', marginLeft: 5 }}>รายการที่รับงานของฉัน</Text>
                    </TouchableOpacity>

                </View>
            </View>

        )
    }

    updateTask = async () => {
        this.setState({
            loading: true,
            refreshing: true
        })
        let db = firebase.firestore()
        let roomid = []
        for (var i = 0; i < this.state.roomList.length; i++) {
            await db.collection("room").doc(this.state.roomList[i].id).collection('info').get().then(function (querySnapshot) {
                querySnapshot.forEach(async function (doc) {
                    // doc.data() is never undefined for query doc snapshots
                    console.log(doc.id, " => ", doc.data());
                    roomid.push(doc.data())
                });
            })
        }
        this.props.orderRoom(roomid)
        this.setState({
            loading: false,
            refreshing: false,
            orderList: roomid
        })
    }
    async componentDidMount() {
        Toast.show({
            text: "ลากลงเพื่ออัพเดทงาน"
        })
        const { status: existingStatus } = await Permissions.getAsync(
            Permissions.NOTIFICATIONS
        );
        let finalStatus = existingStatus;
        if (existingStatus !== 'granted') {
            // Android remote notification permissions are granted during the app
            // install, so this will only ask on iOS
            const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
            finalStatus = status;
        }
        if (finalStatus !== 'granted') {
            return;
        }
        else {
            this.storeDeliverToken()
        }

    }
    storeDeliverToken = async () => {
        let token = await Notifications.getExpoPushTokenAsync();
        console.log('expoToken =>', token)
        let db = firebase.firestore()
        db.collection('expoToken').doc('Deliver').update({
            token: firebase.firestore.FieldValue.arrayUnion(token)
        })
    }
    SelectOrder = async (item) => {
        this.setState({
            loading: true
        })
        let hop = this
        var db = firebase.firestore()
        await db.collection("room").doc(item.OrderRoom).collection('info').doc(item.OrderDate + "-" + item.OrderTime).get().then(function (doc) {
            if (doc.exists) {
                hop.props.navigation.push('DeliverList', { orderList: doc.data() })
                hop.setState({
                    filterMyOrder: false,
                    loading: false
                })
            } else {
                // doc.data() will be undefined in this case
                Toast.show({
                    text: "ไม่มีรายการสั่งนี้แล้ว"
                })
                hop.updateTask()
            }
        }).catch(function (error) {
            console.log("Error getting document:", error);
        });

    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                {
                    this.state.loading &&
                    <View style={{ position: 'absolute', left: 0, top: 0, zIndex: 8, width, height, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(255, 255, 255, 0.8)' }}>
                        <ActivityIndicator color="black" size="large" />
                    </View>

                }
                <FlatList
                    ListHeaderComponent={this.headerComp}
                    data={this.props.orderList.sort(function(a,b) {
                        if(a.DeliverStatus == b.DeliverStatus){
                            return 0
                        }
                        else if(a.DeliverStatus){
                            return 1
                        }
                        else{
                            return -1
                        }
                    })}
                    extraData={this.props}
                    refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.updateTask} />}
                    keyExtractor={(item, i) => item.id}
                    renderItem={({ item, i }) => 
                        <Card style={{ width: width * 0.9, alignSelf: 'center', marginTop: 10 }}>
                            <TouchableOpacity onPress={() => this.SelectOrder(item)} style={{ paddingVertical: 10, flexDirection: 'row', justifyContent: 'space-around', borderTopWidth: 5, borderColor: 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')', }}>
                                <View style={{ flexDirection: 'column', alignItems: 'center' ,borderRightWidth:0.5,padding:10}}>
                                    <Text style={{ fontFamily: 'kanit', color: '#686868', fontSize: 15 }}>ห้อง</Text>
                                    <Text style={{ fontFamily: 'kanit', color: '#686868', fontSize: 50 }}>{item.OrderRoom}</Text>
                                </View>
                                <View style={{ flexDirection: 'column', alignItems: 'flex-start',justifyContent:'center',width:180 }}>
                                    <Text style={{ marginTop: 10, fontFamily: 'kanit',fontSize:12 }}>วันเดือนปี : {item.OrderDate}</Text>
                                    <Text style={{ marginTop: 10, fontFamily: 'kanit',fontSize:12 }}>เวลา : {item.OrderTime}</Text>
                                    <Text style={{ marginTop: 10, fontFamily: 'kanit',fontSize:12 }}>ผู้รับผิดชอบ : {item.DeliverUser == 'empty' ? 'ยังไม่มีผู้รับผิดชอบ' : item.DeliverUser}</Text>
                                    <Text style={{ marginTop: 10, fontFamily: 'kanit', fontSize: 12 }}>สถานะ : {item.DeliverStatus == true ? 'จัดส่งแล้ว' : 'ยังไม่ได้จัดส่ง'}</Text>
                                </View>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon type="FontAwesome" name={item.DeliverUser == 'empty' && item.DeliverStatus == false ? 'question-circle' :item.DeliverUser != 'empty' && item.DeliverStatus == false ?'hourglass-half':'check-circle'} style={{ fontSize: 50, color: item.DeliverUser == 'empty'&& item.DeliverStatus == false  ? '#ebab34' :item.DeliverUser != 'empty'&& item.DeliverStatus == false ?'#03cafc':'#00ad40' }} />
                                </View>
                            </TouchableOpacity>
                        </Card>
                    }
                />
                {
                    this.props.orderList.length == 0 &&
                    <View style={{ flexDirection: 'column', alignItems: 'center', justifyContent: 'center', position: 'absolute', top: '40%', alignSelf: 'center' }}>
                        <Text style={{ marginTop: 25, fontSize: 20, color: '#BFC0C0', fontFamily: 'kanit' }}>ไม่มีรายการที่ถูกสั่ง</Text>
                    </View>
                }

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    }
})

const mapStateToProps = state => {
    return {
        user: state.redux.user,
        roomList: state.redux.roomList,
        orderList: state.redux.orderList
    }
}
const mapDispatchToProps = dispatch => {
    return {
        logIn: (user) => dispatch(logIn(user)),
        orderRoom: (order) => dispatch(orderRoom(order))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DeliverScreen)