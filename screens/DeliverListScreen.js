import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    StatusBar,
    AsyncStorage,
    ActivityIndicator,
    Dimensions,
    Alert,
    Keyboard,
    SafeAreaView
} from 'react-native';
import { Input, Form, Item, Label, Card, CardItem, Body, Icon, Toast } from 'native-base'
import firebase from "firebase"
import '@firebase/firestore'
import { connect } from 'react-redux'
import { FontAwesome } from '@expo/vector-icons'
import HeaderLeft from '../components/headerLeft'
import { logIn, orderRoom } from '../redux/actions/index'
import { FlatList } from 'react-native-gesture-handler';
const { width, height } = Dimensions.get('screen')
class DeliverListScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerStyle: {
                backgroundColor: "#0A2463"
            },
            headerLeft: <TouchableOpacity style={{ padding: 8, marginLeft: 5, flexDirection: 'row', alignItems: 'center', borderRadius: 25 }} onPress={() => navigation.pop()}>
                <FontAwesome name="angle-left" size={20} style={{color:'white'}} />
                <Text style={{ fontSize: 15, marginLeft: 5, fontFamily: 'kanit',color:'white' }}>กลับ</Text>
            </TouchableOpacity>,
            headerTitle: <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}><Text style={{ fontSize: 20, fontFamily: 'kanit', color: 'white' }}>รายการที่ต้องส่ง</Text></View>,
            headerRight: <View></View>
        };
    }
    state = {
        orderList: this.props.navigation.state.params.orderList,
        loading: false
    }
    deliverSave = async () => {
        let db = firebase.firestore()
        let hop = this
        if (this.state.orderList.DeliverUser == this.props.user.name) {
            this.setState({
                loading: true
            })
            db.collection("room").doc(hop.state.orderList.OrderRoom).collection('info').doc(hop.state.orderList.OrderDate + "-" + hop.state.orderList.OrderTime).delete().then(async function () {
                fetch('https://wonderdrink-api.herokuapp.com/orders', {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        orderuser:hop.state.orderList.OrderUser,
                        serviceuser:hop.props.user.name,
                        orderitems:JSON.stringify(hop.state.orderList.OrderList),
                        orderdate:hop.state.orderList.OrderDate,
                        room:hop.state.orderList.OrderRoom,
                        orderstatus:true,
                        ordertime:hop.state.orderList.OrderTime
                    }),
                }).then(response => {
                    response.json()
                }).then(responseJson => {
                    alert('Success',responseJson)
                })
                hop.props.navigation.push('Ack')
                hop.setState({
                    loading: false
                })
            }).catch(function (error) {
                hop.setState({
                    loading: false
                })
                alert("เกิดข้อผิดพลาด กรุณาทำใหม่อีกครั้งในภายหลัง")
                hop.props.navigation.pop()
            });
        }
        else {
            db.collection('room').doc(hop.state.orderList.OrderRoom).collection('info').doc(hop.state.orderList.OrderDate + "-" + hop.state.orderList.OrderTime).update({
                'DeliverUser': hop.props.user.name,
                'OrderList': hop.state.orderList.OrderList,
                'orderStatus': true,
                'OrderUser': hop.state.orderList.OrderUser,
                'OtherOrder': hop.state.orderList.OtherOrder,
                'OrderDate': hop.state.orderList.OrderDate,
                'OrderTime': hop.state.orderList.OrderTime,
                'OrderRoom': hop.state.orderList.OrderRoom,
                'DeliverStatus': false
            })
                .then(async function () {
                    let body = {
                        'DeliverUser': hop.props.user.name,
                        'OrderList': hop.state.orderList.OrderList,
                        'orderStatus': true,
                        'OrderUser': hop.state.orderList.OrderUser,
                        'OtherOrder': hop.state.orderList.OtherOrder,
                        'OrderDate': hop.state.orderList.OrderDate,
                        'OrderTime': hop.state.orderList.OrderTime,
                        'OrderRoom': hop.state.orderList.OrderRoom,
                        'PushToken': hop.state.orderList.PushToken,
                        'DeliverStatus': false
                    }
                    hop.setState({
                        orderList: body,
                    })
                    let roomOrder = []
                    for (var i = 0; i < hop.props.roomList.length; i++) {
                        await db.collection("room").doc(hop.props.roomList[i].id).collection('info').get().then(function (querySnapshot) {
                            querySnapshot.forEach(async function (doc) {
                                // doc.data() is never undefined for query doc snapshots
                                roomOrder.push(doc.data())
                            });
                        })
                    }
                    hop.props.orderRoom(roomOrder)
                    hop.props.navigation.popToTop()
                    console.log('ORDER TOKEN => ', hop.state.orderList.PushToken)
                    fetch('https://exp.host/--/api/v2/push/send', {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            to: hop.state.orderList.PushToken,
                            title: `ห้อง ${hop.state.orderList.OrderRoom} มีผู้จัดส่งแล้ว`,
                            body: `ห้อง ${hop.state.orderList.OrderRoom} ถูกจัดส่งโดย ${hop.props.user.name}`,
                            sound: 'default'
                        }),
                    })

                })
                .catch(e => {
                    alert(e)
                })
        }
    }
    headerComp = () => {
        return (
            <View style={{ borderTopWidth: 55, borderColor: '#D4F3FF' }}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={{ fontSize: 60, fontFamily: 'kanit', color: '#0A2463' }}>{this.state.orderList.OrderRoom}</Text>
                </View>
            </View>
        )
    }
    footerComp = () => {
        return (
            <View style={{ marginHorizontal: 10, marginTop: 40 }}>
                <View style={{ width: width * 0.9, borderBottomWidth: 0.5, alignSelf: 'center', marginBottom: 20 }}>
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', margin: 5 }}>
                    <Text style={{ fontSize: 15, fontFamily: 'kanit', color: '#0A2463' }}>วันที่จัดส่ง:</Text>
                    <View style={{ marginLeft: 10, borderBottomWidth: 0.5 }}>
                        <Text style={{ fontSize: 15, fontFamily: 'kanit', color: 'grey' }}>{this.state.orderList.OrderDate}</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', margin: 5 }}>
                    <Text style={{ fontSize: 15, fontFamily: 'kanit', color: '#0A2463' }}>เวลาที่จัดส่ง:</Text>
                    <View style={{ marginLeft: 10 }}>
                        <Text style={{ fontSize: 15, color: 'grey', fontFamily: 'kanit' }}>{this.state.orderList.OrderTime}</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', margin: 5 }}>
                    <Text style={{ fontSize: 15, fontFamily: 'kanit', color: '#0A2463' }}>ชื่อผู้สั่ง:</Text>
                    <View style={{ marginLeft: 10 }}>
                        <Text style={{ fontSize: 15, color: 'grey', fontFamily: 'kanit' }}>{this.state.orderList.OrderUser}</Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', margin: 5 }}>
                    <Text style={{ fontSize: 15, fontFamily: 'kanit', color: '#0A2463' }}>ชื่อผู้รับผิดชอบ:</Text>
                    <View style={{ marginLeft: 10 }}>
                        <Text style={{ fontSize: 15, color: 'grey', fontFamily: 'kanit' }}>{this.state.orderList.DeliverUser == 'empty' ? 'ยังไม่มีผู้รับผิดชอบ' : this.state.orderList.DeliverUser}</Text>
                    </View>
                </View>
                <Text style={{ fontSize: 15, margin: 5, fontFamily: 'kanit', color: '#0A2463' }}>รายการเพิ่มเติม</Text>
                <View style={styles.textAreaContainer}>
                    <TextInput
                        style={styles.textArea}
                        multiline={true}
                        numberOfLines={4}
                        placeholder={'ไม่ได้ระบุ'}
                        placeholderTextColor="grey"
                        editable={false}
                        value={this.state.orderList.OtherOrder} />
                </View>
            </View>
        )
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                {
                    this.state.loading &&
                    <View style={{ position: 'absolute', left: 0, top: 0, zIndex: 8, width, height, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(255, 255, 255, 0.8)' }}>
                        <ActivityIndicator color="black" size="large" />
                    </View>
                }
                <FlatList
                    data={this.state.orderList.OrderList}
                    extraData={this.state}
                    ListFooterComponent={this.footerComp}
                    keyExtractor={(item, index) => index.toString()}
                    ListHeaderComponent={this.headerComp}
                    renderItem={({ item, index }) =>
                        <View key={index}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', margin: 10 }}>
                                <Text style={{ fontSize: 15, color: '#0A2463', fontFamily: 'kanit' }}>{item.OrderName}</Text>
                                <Text style={{ fontSize: 15, fontFamily: 'kanit', color: '#0A2463' }}>{item.Amount} ชิ้น</Text>
                            </View>
                        </View>
                    }
                />
                {
                    (this.state.orderList.DeliverUser == 'empty' || this.state.orderList.DeliverUser == this.props.user.name) &&
                    <View style={{ justifyContent: 'center', alignitems: 'center', height: 55, backgroundColor: '#D4F3FF' }}>
                        <TouchableOpacity onPress={() => this.deliverSave()} style={{ backgroundColor: this.state.orderList.DeliverUser == this.props.user.name ? '#fcba03' : 'white', justifyContent: 'center', alignItems: 'center', padding: 5, width: width * 0.5, alignSelf: 'center', borderRadius: 30, flexDirection: 'row' }}>

                            <Text style={{ fontSize: 16, color: this.state.orderList.DeliverUser == this.props.user.name ? 'white' : '#0A2463', fontFamily: 'kanit' }}>{this.state.orderList.DeliverUser == this.props.user.name ? 'เสร็จงาน' : 'รับงาน'}</Text>
                        </TouchableOpacity>
                    </View>
                }
            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    textAreaContainer: {
        borderColor: '#666',
        borderWidth: 1,
        padding: 5,
        marginTop: 10
    },
    textArea: {
        height: 150,
        textAlignVertical: 'top'
        , fontFamily: 'kanit',
        fontSize: 15
    }
})
const mapStateToProps = state => {
    return {
        user: state.redux.user,
        roomList: state.redux.roomList,
        orderList: state.redux.orderList,
        token: state.redux.token
    }
}
const mapDispatchToProps = dispatch => {
    return {
        logIn: (user) => dispatch(logIn(user)),
        orderRoom: (order) => dispatch(orderRoom(order))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(DeliverListScreen)