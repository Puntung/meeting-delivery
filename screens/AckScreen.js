import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    StatusBar,
    AsyncStorage,
    ActivityIndicator,
    Dimensions,
    Alert,
    Keyboard,
    SafeAreaView
} from 'react-native';
import firebase from "firebase"
import '@firebase/firestore'
import { connect } from 'react-redux'
import { FontAwesome } from '@expo/vector-icons'
import HeaderLeft from '../components/headerLeft'
import { logIn, orderRoom } from '../redux/actions/index'
import { FlatList } from 'react-native-gesture-handler';
const { width, height } = Dimensions.get('screen')
class AckScreen extends React.Component {
    static navigationOptions = {
        header: null,
    };
    state = {
        loading: false
    }

    backToMain = async () => {
        this.setState({
            loading:true,
        })
        let db = firebase.firestore()
        let roomid = []
        for (var i = 0; i < this.props.roomList.length; i++) {
            await db.collection("room").doc(this.props.roomList[i].id).collection('info').get().then(function (querySnapshot) {
                querySnapshot.forEach(async function (doc) {
                    // doc.data() is never undefined for query doc snapshots
                    console.log(doc.id, " => ", doc.data());
                    roomid.push(doc.data())
                });
            })
        }
        this.props.orderRoom(roomid)
        this.props.navigation.popToTop()
        this.setState({
            loading:false,
        })
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                {
                    this.state.loading &&
                    <View style={{ position: 'absolute', left: 0, top: 0, zIndex: 8, width, height, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(255, 255, 255, 0.8)' }}>
                        <ActivityIndicator color="black" size="large" />
                    </View>
                }
                <View style={{ justifyContent: 'center', alignItems: 'center',backgroundColor:'white',borderRadius:50,flex:0.8,width }}>
                    <Text style={{fontSize:35,fontFamily:'kanit'}}>จัดส่งสำเร็จแล้ว!</Text>
                    <FontAwesome name="check-circle" style={{ fontSize: 350, color: '#00B524' }} />
                    <TouchableOpacity onPress={()=>this.backToMain()} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingVertical: 15,paddingHorizontal:30, borderRadius: 45 ,marginTop:10,backgroundColor:'#fcba03'}}>
                        <Text style={{fontSize:20,fontFamily:'kanit',color:'white'}}>กลับสู่หน้าหลัก</Text>
                    </TouchableOpacity>
                </View>

            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#D4F3FF',
    },
    textAreaContainer: {
        borderColor: '#666',
        borderWidth: 1,
        padding: 5,
        marginTop: 10
    },
    textArea: {
        height: 150,
        textAlignVertical: 'top'
    }
})
const mapStateToProps = state => {
    return {
        user: state.redux.user,
        roomList: state.redux.roomList,
        orderList: state.redux.orderList
    }
}
const mapDispatchToProps = dispatch => {
    return {
        logIn: (user) => dispatch(logIn(user)),
        orderRoom: (order) => dispatch(orderRoom(order))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AckScreen)