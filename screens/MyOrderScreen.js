import React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    TextInput,
    StatusBar,
    AsyncStorage,
    ActivityIndicator,
    Dimensions,
    Alert,
    Keyboard,
    SafeAreaView,
} from 'react-native';
import { Input, Form, Item, Label, Card, CardItem, Body, Icon, Toast } from 'native-base'
import firebase from "firebase"
import '@firebase/firestore'
import { connect } from 'react-redux'
import HeaderLeft from '../components/headerLeft'
import { logIn, orderRoom } from '../redux/actions/index'
import { FlatList } from 'react-native-gesture-handler';
import { FontAwesome } from '@expo/vector-icons'
const { width, height } = Dimensions.get('screen')
class MyOrderScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerStyle: {
                backgroundColor: "#610085"
            },
            headerLeft: <TouchableOpacity style={{ padding: 8, marginLeft: 5, flexDirection: 'row', alignItems: 'center',  borderRadius: 25 }} onPress={() => navigation.pop()}>
                <FontAwesome name="angle-left" size={20} style={{color:'white'}}/>
                <Text style={{ fontSize: 15, marginLeft: 5, fontFamily: 'kanit' ,color:'white'}}>กลับ</Text>
            </TouchableOpacity>,
            headerTitle: <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}><Text style={{ fontSize: 20, fontFamily: 'kanit', color: 'white' }}>ห้องประชุม</Text></View>,
            headerRight: <View></View>
        };
    }
    state = {
        roomList: this.props.roomList,
        loading: false,
        orderList: this.props.orderList
    }
    logOutFunc = async () => {
        let code = await AsyncStorage.getItem('code')
        if (code != null) {
            await AsyncStorage.removeItem('code')
            this.props.logIn(null)
            this.props.navigation.replace('Home')
        }
        else {
            this.props.logIn(null)
            this.props.navigation.replace('Home')
        }
    }
    headerComp = () => {
        return (
            <View style={{ backgroundColor: '#db63aa' }}>
                <Image source={require('../assets/images/bgOrder2.png')} style={{width,height:200,position:'absolute',zIndex:-1,opacity:0.1}} />
                <View style={{ justifyContent: 'center', flexDirection: 'row', marginTop: 20, height: 200 }}>
                    <Text style={{ fontFamily: 'kanit', fontSize: 40, color: 'white' }}>รายการของฉัน</Text>
                </View>
                <View style={{ bottom: -15, position: 'absolute', flexDirection: 'row', justifyContent: 'space-between', borderRadius: 20, backgroundColor: 'white', width: width, paddingBottom: 50, paddingTop: 20 }}>
                    <Text style={{ fontSize: 14, fontFamily: 'kanit', marginLeft: 10 }}>จำนวนงาน : {this.props.orderList.length}</Text>


                </View>
            </View>
        )
    }
    async componentDidMount() {
        this.setState({
            loading: true
        })
        let db = firebase.firestore()
        let roomOrder = []
        for (var i = 0; i < this.props.roomList.length; i++) {
            await db.collection("room").doc(this.props.roomList[i].id).collection('info').get().then(function (querySnapshot) {
                querySnapshot.forEach(async function (doc) {
                    // doc.data() is never undefined for query doc snapshots
                    roomOrder.push(doc.data())
                });
            })
        }

        this.props.orderRoom(roomOrder)
        console.log('OrderList =>', this.props.orderList)
        this.setState({
            orderList: roomOrder,
            loading: false
        })
    }
    SelectOrder = async (item) => {
        this.setState({
            loading: true
        })
        let hop = this
        var db = firebase.firestore()
        await db.collection("room").doc(item.OrderRoom).collection('info').doc(item.OrderDate + "-" + item.OrderTime).get().then(function (doc) {
            if (doc.exists) {
                hop.props.navigation.push('MyOrderList', { orderList: doc.data() })
                hop.setState({
                    loading: false
                })
            } else {
                // doc.data() will be undefined in this case
                Toast.show({
                    text: "ผิดพลาด กรุณาลากลงเพื่ออัพเดทงาน"
                })
                hop.setState({
                    loading: false
                })
            }
        }).catch(function (error) {
            console.log("Error getting document:", error);
        });

    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                {
                    this.state.loading &&
                    <View style={{ position: 'absolute', left: 0, top: 0, zIndex: 8, width, height, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(255, 255, 255, 0.8)' }}>
                        <ActivityIndicator color="black" size="large" />
                    </View>

                }
                <FlatList
                    ListHeaderComponent={this.headerComp}
                    data={this.props.orderList}
                    extraData={this.props}
                    keyExtractor={(item, i) => item.id}
                    renderItem={({ item, i }) => {
                        if (item.OrderUser == this.props.user.name) {
                            return (
                                <Card style={{ width: width * 0.9, alignSelf: 'center', marginTop: 10 }}>
                                    <TouchableOpacity onPress={() => this.SelectOrder(item)} style={{ paddingVertical: 10, flexDirection: 'row', justifyContent: 'space-around', borderTopWidth: 5, borderColor: 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')', }}>
                                        <View style={{ flexDirection: 'column', alignItems: 'center', borderRightWidth: 0.5, padding: 10 }}>
                                            <Text style={{ fontFamily: 'kanit', color: '#686868', fontSize: 15 }}>ห้อง</Text>
                                            <Text style={{ fontFamily: 'kanit', color: '#686868', fontSize: 50 }}>{item.OrderRoom}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'column', alignItems: 'flex-start', justifyContent: 'center', width: 180 }}>
                                            <Text style={{ marginTop: 10, fontFamily: 'kanit', fontSize: 12 }}>วันเดือนปี : {item.OrderDate}</Text>
                                            <Text style={{ marginTop: 10, fontFamily: 'kanit', fontSize: 12 }}>เวลา : {item.OrderTime}</Text>
                                            <Text style={{ marginTop: 10, fontFamily: 'kanit', fontSize: 12 }}>ผู้รับผิดชอบ : {item.DeliverUser == 'empty' ? 'ยังไม่มีผู้รับผิดชอบ' : item.DeliverUser}</Text>
                                            <Text style={{ marginTop: 10, fontFamily: 'kanit', fontSize: 12 }}>สถานะ : {item.DeliverStatus == true ? 'จัดส่งแล้ว' : 'ยังไม่ได้จัดส่ง'}</Text>
                                        </View>
                                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                            <Icon type="FontAwesome" name={item.DeliverUser == 'empty' && item.DeliverStatus == false ? 'question-circle' : item.DeliverUser != 'empty' && item.DeliverStatus == false ? 'hourglass-half' : 'check-circle'} style={{ fontSize: 50, color: item.DeliverUser == 'empty' && item.DeliverStatus == false ? '#ebab34' : item.DeliverUser != 'empty' && item.DeliverStatus == false ? '#03cafc' : '#00ad40' }} />
                                        </View>
                                    </TouchableOpacity>
                                </Card>
                            )
                        }
                    }
                    }
                />
                {
                    this.props.orderList.length == 0 &&
                    <View style={{ flexDirection: 'column', alignItems: 'center', justifyContent: 'center', position: 'absolute', top: '40%', alignSelf: 'center' }}>
                        <Text style={{ marginTop: 25, fontSize: 20, color: '#BFC0C0', fontFamily: 'kanit' }}>ไม่มีรายการที่ถูกสั่ง</Text>
                    </View>
                }
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    }
})

const mapStateToProps = state => {
    return {
        user: state.redux.user,
        roomList: state.redux.roomList,
        orderList: state.redux.orderList,
        update: state.redux.update
    }
}
const mapDispatchToProps = dispatch => {
    return {
        logIn: (user) => dispatch(logIn(user)),
        orderRoom: (order) => dispatch(orderRoom(order))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyOrderScreen)